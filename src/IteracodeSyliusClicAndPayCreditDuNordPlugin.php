<?php

declare(strict_types=1);

namespace Iteracode\SyliusClicAndPayCreditDuNordPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class IteracodeSyliusClicAndPayCreditDuNordPlugin extends Bundle
{
    use SyliusPluginTrait;
}

<?php

declare(strict_types=1);

namespace Iteracode\SyliusClicAndPayCreditDuNordPlugin\Payum\Action;

use Iteracode\SyliusClicAndPayCreditDuNordPlugin\Payum\SyliusApi;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Reply\HttpPostRedirect;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;
use Payum\Core\Request\Capture;
use Psr\Http\Message\ResponseInterface;

final class CaptureAction implements ActionInterface, ApiAwareInterface
{
    /** @var Client */
    private $client;
    /** @var SyliusApi */
    private $api;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var SyliusPaymentInterface $payment */
        $payment = $request->getModel();

        /** @var TokenInterface $token */
        $token = $request->getToken();

        $data = $_POST;

        if (isset($data['vads_trans_status'])) {
            /*if ($data['vads_trans_status'] == 'AUTHORISED' || $data['vads_trans_status'] == 'CAPTURED')
            {
                //$request->markCaptured();
                $payment->setState('completed');
            }
            else {
                //$request->markFailed();
                $payment->setState('failed');
            }*/
            $payment->setDetails(['vads_trans_status' => $data['vads_trans_status']]);
        }
        else {
            $this->getResponseFromBank($payment, $token);
        }
    

       /* try {
            $response = $this->getResponseFromBank($payment);

        } catch (RequestException $exception) {
            $response = $exception->getResponse();
        } finally {
            $payment->setDetails(['status' => $response->getStatusCode()]);
        }*/
    }

    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof SyliusPaymentInterface
        ;
    }

    public function setApi($api): void
    {
        if (!$api instanceof SyliusApi) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . SyliusApi::class);
        }

        $this->api = $api;
    }

    private function getResponseFromBank(SyliusPaymentInterface $payment, $token): ResponseInterface
    {
        $params = [
            'vads_action_mode' => 'INTERACTIVE',
            'vads_amount' => $payment->getAmount(),
            'vads_ctx_mode' => 'PRODUCTION',
            'vads_currency' => 978,
            'vads_page_action' => 'PAYMENT',
            'vads_payment_config' => 'SINGLE',
            'vads_site_id' => '20455687',
            'vads_trans_date' => date('YmdHis', time() - 3600),
            'vads_trans_id' => str_pad((String)$payment->getId(), 6, '0', STR_PAD_LEFT),
            'vads_version' => 'V2',
            'vads_capture_delay' => 0,
            'vads_validation_mode' => 0,
           // 'vads_return_mode' => 'GET',
           // 'vads_url_return' => $token->getTargetUrl(),
           // 'vads_url_return' => $token->getAfterUrl(),
            'vads_url_check' => $token->getTargetUrl()
        ];
        $params['signature'] = $this->getSignature($params, $this->api->getApiKey());

        $headers = ['application/x-www-form-urlencoded'];

        throw new HttpPostRedirect(
            'https://clicandpay.groupecdn.fr/vads-payment/',
            $params,
            200,
            $headers
        );

        //return $response;
    }

    private function getSignature($params, $key): string
    {
        /**
         * Fonction qui calcule la signature.
         * $params : tableau contenant les champs à envoyer dans le formulaire.
         * $key : clé de TEST ou de PRODUCTION
         */
        //Initialisation de la variable qui contiendra la chaine à chiffrer
        $contenu_signature = "";

        //Tri des champs par ordre alphabétique
        ksort($params);
        foreach ($params as $nom => $valeur) {

            //Récupération des champs vads_
            if (substr($nom, 0, 5)=='vads_') {

                //Concaténation avec le séparateur "+"
                $contenu_signature .= $valeur."+";
            }
        }
        //Ajout de la clé en fin de chaine
        $contenu_signature .= $key;

        //Encodage base64 de la chaine chiffrée avec l'algorithme HMAC-SHA-256
        $signature = base64_encode(hash_hmac('sha256', $contenu_signature, $key, true));

        return $signature;
    }
}

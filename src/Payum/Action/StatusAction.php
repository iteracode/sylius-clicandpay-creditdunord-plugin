<?php

declare(strict_types=1);

namespace Iteracode\SyliusClicAndPayCreditDuNordPlugin\Payum\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetStatusInterface;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;

final class StatusAction implements ActionInterface
{

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var SyliusPaymentInterface $payment */
        $payment = $request->getFirstModel();
        $details = $payment->getDetails();

        //$data = $_POST;

        if (isset($details['vads_trans_status'])) {
            if ($details['vads_trans_status'] == 'AUTHORISED' || $details['vads_trans_status'] == 'CAPTURED')
            {
                $request->markCaptured();
                return;
            }
            else {
                $request->markFailed();
                return;
            }
        }

       // throw new \Exception(serialize($_POST));
        
    }

    public function supports($request): bool
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getFirstModel() instanceof SyliusPaymentInterface
        ;
    }
}

<?php

declare(strict_types=1);

namespace Iteracode\SyliusClicAndPayCreditDuNordPlugin\Payum;

use Iteracode\SyliusClicAndPayCreditDuNordPlugin\Payum\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class SyliusPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'clic_and_pay_credit_du_nord',
            'payum.factory_title' => 'Clic and Pay Crédit du Nord',
            'payum.action.status' => new StatusAction(),
        ]);

        $config['payum.api'] = function (ArrayObject $config) {
            //return new SyliusApi($config['api_key']);
            return new SyliusApi($config['api_key']);
        };
    }
}
